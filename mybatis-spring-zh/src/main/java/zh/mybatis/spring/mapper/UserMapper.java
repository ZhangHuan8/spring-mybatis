package zh.mybatis.spring.mapper;

import org.springframework.stereotype.Repository;
import zh.mybatis.spring.model.SysUser;

/**
 * Created by huan on 2018/1/13.
 */
@Repository
public interface UserMapper {
    SysUser selectById(Long id);
}
