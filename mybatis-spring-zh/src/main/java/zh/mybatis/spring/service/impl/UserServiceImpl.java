package zh.mybatis.spring.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zh.mybatis.spring.mapper.UserMapper;
import zh.mybatis.spring.model.SysUser;
import zh.mybatis.spring.service.UserService;

/**
 * Created by huan on 2018/1/6.
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public SysUser selectById(Long id) {
        return userMapper.selectById(1L);
    }
}
