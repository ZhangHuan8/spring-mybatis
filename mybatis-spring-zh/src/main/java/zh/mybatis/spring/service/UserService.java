package zh.mybatis.spring.service;

import zh.mybatis.spring.model.SysUser;

import java.util.List;

/**
 * Created by huan on 2018/1/6.
 */
public interface UserService {
    SysUser selectById(Long id);
}
