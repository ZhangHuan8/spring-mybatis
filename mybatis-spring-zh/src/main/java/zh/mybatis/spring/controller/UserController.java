package zh.mybatis.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import zh.mybatis.spring.model.SysUser;
import zh.mybatis.spring.service.UserService;

/**
 * Created by huan on 2018/1/13.
 */
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = {"/selectById"})
    @ResponseBody
    public SysUser selectById(){
        return userService.selectById(1L);
    }
}
